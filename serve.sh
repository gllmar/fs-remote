#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

SEND=127.0.0.1:10011
LOAD="$SCRIPTPATH"/fs-remote.json
OSCPORT=10010
WEBPORT=8080

cd "$SCRIPTPATH"

echo "	http://$HOSTNAME.local:$WEBPORT"
echo " $LOAD"
node open-stage-control/index.js -s $SEND -l $LOAD -o $OSCPORT -p $WEBPORT --no-qrcode

